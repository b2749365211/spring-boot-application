package com.zuitt.example.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s!", name);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String name){
		return String.format("Hi %s!", name);
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "guy") String name, int age){
		return String.format("Hello " + name + "! Your age is " + age + ".");
	}

	@GetMapping("/shape")
	public String shape(@RequestParam(value = "shape", defaultValue = "error") String shapeName, int numberOfSides){
		return String.format("Your shape is " + shapeName + ". It has " + numberOfSides + " sides.");
	}

}