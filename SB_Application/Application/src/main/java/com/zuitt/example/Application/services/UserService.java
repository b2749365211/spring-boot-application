package com.zuitt.example.Application.services;

import com.zuitt.example.Application.models.User;

import java.util.Optional;
public interface UserService {
    void createUser(User user);

    Optional<User> findByUsername(String name);
}
